#
#  JJD, 12/11/2019
#
#  Provides access to Domoticz objects via classes
#
#    GPL v2 Licence
#
# To retrieve idx of devices when unknown
# http://192.168.1.26:8080/json.htm?type=command&param=getlightswitches
#
# API documentation
# https://www.domoticz.com/wiki/Domoticz_API/JSON_URL%27s




import urllib, urllib.request, json
from socket import timeout
from urllib.error import HTTPError, URLError




#-----------------------------------------------------------------------------------------------------------
#            BASE DOMOTICZ CLASS
#-----------------------------------------------------------------------------------------------------------



class DOMOTICZ_DEVICE():
    '''Generic device for Domoticz'''
    def __init__(self, urlbase, username, passwd, IDx, verbose=False, dry_run=False):
        self.urlbase = urlbase
        self.username= username
        self.passwd  = passwd
        self.IDx     = IDx
        self.name    = "domoticz device"
        self.verbose = verbose
        self.dry_run = dry_run
        
    def __repr__(self):
        return "{} - ID={}".format(self.name, self.IDx )

    def log(self, t):
        if self.verbose:
            print( '-- {} -- {}'.format( self, t )   )
    def error(self, t):
        print( '-ERROR- {} -- {}'.format( self, t )   )
    
    def send(self, url ):
        self.log( "sending {}".format(url) )
        
        if self.dry_run:
            # Not sending request if in dry_run mode
            return None
        else:
            resp = None
            try:
                f = urllib.request.urlopen(url, timeout=1)
                resp = f.read()
                f.close()
            except (HTTPError, URLError) as error:
                self.error('Error - Wrong url: {}'.format(url) )
                return resp
            except timeout:
                self.error('Socket timed out')
            else:
                self.log('Access successful')
                return resp            
   

#-----------------------------------------------------------------------------------------------------------
#            CHILD CLASSES
#-----------------------------------------------------------------------------------------------------------



class SWITCH(DOMOTICZ_DEVICE):
    '''Toggles a Domoticz switch'''
    def __init__(self, urlbase, username, passwd, IDx, verbose=False, dry_run=False):
        DOMOTICZ_DEVICE.__init__(self, urlbase, username, passwd, IDx, verbose, dry_run)
        self.name    = "ON/OFF switch"
        
    def set(self, state ):
        self.log( "sending {}".format(state) )
        def onoff(state):
            if state == 1: return 'On'
            if state == 0: return 'Off'
        
        url = '{}/json.htm?username={}&password={}&type=command&param=switchlight&idx={}&switchcmd={}'.format(self.urlbase, self.username, self.passwd, self.IDx, onoff( state ))

        resp = self.send(url)
        return resp

    def get(self):
        def ONOFF(status):
            if status == 'On':  return True
            if status == 'Off': return False
        
        url = '{}/json.htm?username={}&password={}&type=devices&rid={}'.format(self.urlbase, self.username, self.passwd, str(self.IDx))
        resp = self.send(url)
        return ONOFF(json.loads( resp )['result'][0]['Status'])

    def refresh(self, state):
        self( not(state) )
        self( state )       





class SELECTOR_SWITCH(DOMOTICZ_DEVICE):
    def __init__(self, urlbase, username, passwd, IDx, dic, verbose=False, dry_run=False ):
        DOMOTICZ_DEVICE.__init__(self, urlbase, username, passwd, IDx, verbose, dry_run)
        self.name    = "selector switch"
        self.dic     = dic

    def get(self):
        url = '{}/json.htm?username={}&password={}&type=devices&rid={}'.format(self.urlbase, self.username, self.passwd, str(self.IDx))
        resp = self.send(url)
        return self.dic[json.loads( resp )['result'][0]['Level'] ]

    def set(self, Label):
        '''Sets takes a label (e.g. 'Level1'). It searches in the dictionnary for the associated level'''
        level = list(self.dic.keys())[list(self.dic.values()).index(Label)]
        url = '{}/json.htm?username={}&password={}&type=command&param=switchlight&idx={}&switchcmd=Set%20Level&level={}'.format(self.urlbase, self.username, self.passwd, str(self.IDx), level)
        resp = self.send(url)
        return resp


class SENSOR(DOMOTICZ_DEVICE):
    '''Reads a temperature sensor'''
    def __init__(self, urlbase, username, passwd, IDx, verbose=False, dry_run=False ):
        DOMOTICZ_DEVICE.__init__(self, urlbase, username, passwd, IDx, verbose, dry_run)
        self.name    = "Temp sensor"
        
    def get(self):
        '''Get temp and status'''
        url = '{}/json.htm?username={}&password={}&type=devices&rid={}'.format(self.urlbase, self.username, self.passwd, str(self.IDx))
        resp = json.loads( self.send(url) )
        return resp['result'][0]['Temp'], resp['status']




class THERMOSTAT(DOMOTICZ_DEVICE):
    '''Sets a thermostat'''
    def __init__(self, urlbase, username, passwd, IDx, verbose=False, dry_run=False ):
        DOMOTICZ_DEVICE.__init__(self, urlbase, username, passwd, IDx, verbose, dry_run)
        self.name    = "Virtual thermostat"
        
    def set(self, setpoint):
        '''Set temp target'''
        url = '{}/json.htm?username={}&password={}&type=command&param=setsetpoint&idx={}&setpoint={}'.format(self.urlbase, self.username, self.passwd, self.IDx, setpoint )
        resp = json.loads( self.send(url) )
        return resp


class NOTIFICATION(DOMOTICZ_DEVICE):
    '''Send a notification'''
    def __init__(self, urlbase, username, passwd, IDx, verbose=False, dry_run=False ):
        DOMOTICZ_DEVICE.__init__(self, urlbase, username, passwd, IDx, verbose, dry_run)
        self.name    = "notification"
        
    def notify(self, subject, body):
        '''Send message'''
        url = '{}/json.htm?username={}&password={}&type=command&param=sendnotification&{}'.format(self.urlbase, self.username, self.passwd, urllib.parse.urlencode({ 'subject' : subject, 'body':body}))
        return self.send(url)


if __name__ == '__main__':
    # Tests to be placed here
    pass
