#-----------------------------------------------------------------------------------------------------------
#                                SCHEDULE SCRIPT
#-----------------------------------------------------------------------------------------------------------
#
#  Drive various Domoticz objects (switches, ..) according to a schedule entered as a spreadsheet
#   ___from a separate python process___
#
#  JJD, 27/07/2019,  GPL v2 Licence
#
#
#!! Dependencies: 
#!!  - pyexcel_ods   : https://pypi.org/project/pyexcel-ods/
#!!  - Parameters.py : Do not forget to fill up the variables: urlbase, username, passwd  
#!!
#!! In order to test the script manually, type "python3 Schedule.py"
#!! 
#!! Insert following in crontab to run every hour:
#!! 01 * * * * python3 /home/domoticz/Schedule.py > /tmp/Schedule.log
#!!
#!! 

#CHANGE LOG...............................................................
version = "0.4"
#v0.2: 27/10/2019: Modified spreadsheet and script accordingly
#v0.3: 31/10/2019: Rewrote conditions handling to enable winner display
#v0.4: 11/11/2019: Added some level of error handling and SMS notification



import datetime, time, sys, pyexcel_ods

from Parameters import *  # Parameters for the script, entered as python variables
from Domoticz   import *  # Side script that talks to the Domoticz instances




#-----------------------------------------------------------------------------------------------------------
#            CONTAINER CLASS
#-----------------------------------------------------------------------------------------------------------


class CONTAINER():
    ''' Base class. Contains common functions (e.g. log, for error logging

        Also a simple container class. Manages a collection of objects, and provides simple methods to
        browse through them (iteration, indexing)'''

    def __init__(self, verbose=False):
        self.STORE   =[]
        self.i       = 0
        self.name    = 'container'
        self.verbose = verbose

    def log(self, t, level="log", errortype=None):
        '''Prints debug information
            level entered as a text, can be log, warning or error'''
        if not level in ['log', 'warning', 'error']:
            raise ValueError('level has to be log, warning or error')

        # Message, to be printed on screen or sent by SMS
        message = '{} -- {} -- {}'.format(level, self.name, t)

        # Printing message to the user
        if self.verbose or level in ['warning','error']:
            print(message)

        # On warning or error, notify by SMS
        if level in ['warning', 'error']:
            self.log('sending SMS')
            notification.notify("Schedule script", message)
        
        # On error, raise exception 
        if level == 'error': raise errortype(t)

    def __iter__(self):
        self.IteratorIndex = 0   
        return self
    
    def __next__(self):
        if self.IteratorIndex == len(self.STORE):
            raise StopIteration
        else:
            self.IteratorIndex += 1
            return self.STORE[self.IteratorIndex - 1 ]       

    def __len__(self):
        return len(self.STORE)

    def __call__(self, ID):
        return self.STORE[ID]






#-----------------------------------------------------------------------------------------------------------
#            CONDITIONS CLASS
#-----------------------------------------------------------------------------------------------------------


class CONDITIONS(CONTAINER):
    '''Conditions class. Stores all up-to-date conditions, and enables decision on the current conditions'''
    def __init__(self, defaultConditionsPriority=0, verbose=False):
        CONTAINER.__init__(self)
        self.name         = 'conditions'
        
        # Creating empty store
        self.conditions                 = {}
        self.defaultConditionsPriority  = defaultConditionsPriority
        self.verbose                    = verbose
        self.update_defaults()

    def __repr__(self):
        out = (' '*25 + 'CONDITION')[-20:] + '\t T/F \tPRIORITY \t DEFAULT\n'
        for condition in self.conditions:
            out += (' '*25 + condition)[-20:] + '\t' + str(self.value(condition)) + '\t' + str(self.priority(condition)) + '\t' + str(self.default(condition)) + '\n'
        return out

    def checkConditionExists(self, name):
        if name not in self.conditions:
            self.log("Unknown condition: " + name, level="error", errortype=KeyError )
    
    def value(self, name)   :
        self.checkConditionExists(name)
        return self.conditions[name]['value']
    
    def priority(self, name):
        self.checkConditionExists(name)
        return self.conditions[name]['priority']
    
    def default(self, name) :
        self.checkConditionExists(name)
        return self.conditions[name]['default']
    
    def update_condition(self, name, value, priority, default=False):
        '''Updates an existing condition or creates a new condition'''
        if name in self.conditions:
            self.log("updating condition " + name )   # Just logging the update
        
        self.conditions[name] = {'value':value, 'priority':priority, 'default':default}
    
    def update_defaults(self):
        '''Creates or updates Weekdays, WD, WE conditions'''
        self.now = datetime.datetime.today()
        today    = self.now.weekday()
        DAYS     = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        for dayID in range(len(DAYS)):
            self.update_condition( DAYS[dayID], dayID == today, self.defaultConditionsPriority, default=True  )
            
        self.update_condition( 'WE', today == 5 or today == 6     , self.defaultConditionsPriority, default=True   )
        self.update_condition( 'WD', not(today == 5 or today == 6), self.defaultConditionsPriority, default=True   )

    def evaluate(self, conditions ):
        '''Evaluates a set of conditions and returns value, priority, winner'''
        if conditions == []:
            # No argument given. We return True with a priority of -1
            return  {'priority':-1, 'winner':'()'}
        if type(conditions) == type([]):
            # Given a set of conditions, we have to evaluate all of them
            # Amongst those that are true, we need to find the one with the highest priority
            winner     = ""
            value      = False
            priority   = -1
            for name in conditions:
                if self.value(name) == True:
                    if self.priority(name) > priority:
                        winner   = name
                        value    = True
                        priority = self.priority(name)

            if value == False:
                return None
            else:            
                return {'priority':priority, 'name':winner}
      
    def __len__(self):
        '''Returns the number of --user-- conditions entered. Amount of default conditions is constant'''
        return sum([not(self.default(name)) for name in self.conditions] )

    def TrueConditions(self):
        '''Returns a list of TRUE conditions, for debug'''
        return [name for name in self.conditions if self.value(name) ]



#-----------------------------------------------------------------------------------------------------------
#            DEVICE CLASS
#-----------------------------------------------------------------------------------------------------------



class DEVICE(CONTAINER):
    '''Holds all info for a particular device (schedules and conditions) '''
    def __init__(self, name, ID, object, mode, inverted, schedules, verbose=False):
        CONTAINER.__init__(self)
        self.name      = name
        self.ID        = ID
        self.object    = object
        self.mode      = mode
        self.inverted  = inverted
        self.schedules = schedules
        self.verbose   = verbose

    def __repr__(self):
        return self.name 

    @property
    def state(self):
        '''Returns current state of the device. Defined as a property'''
        if self.mode   == 'off'  : return 0
        elif self.mode == 'on'   : return 1       
        elif self.mode == 'manual': return None

        elif self.mode == 'prog':
        # Program mode, return schedule value
            winner =  self.winningSchedule()
            if winner == None:    return None
            else:                 return self.evaluateSchedule(winner['i'])

        else:
            self.log("Unknown mode: " + self.mode, level='warning', errortype=ValueError )
    
    def winningSchedule(self):
        '''Find the highest priority schedule amongst all declared'''
        # Evaluating conditions for all schedules (creating 'winner' entry)
        for schedule in self.schedules:
            schedule['winner'] = self.conditions.evaluate(schedule['conditions'])

        # Finding highest priority
        count = 0         # counting number of schedules
        index = -1
        priority = -1
        winner = ''
        
        for schedule in self.schedules:
            count += 1
            
            if schedule['winner'] != None and schedule['winner']['priority'] > priority:
                    index    = schedule['i']
                    priority = schedule['winner']['priority']
                    winner   = schedule['winner']['name']

        # Returning winner
        if count == 0:
            # No schedule provided, weird
            self.log("No schedule provided", level='warning')
            return None

        elif count == 1 and self.schedules[0]['conditions'] == []:
            # Only one schedule with no conditions, always true !
            return {'i':0, 'priority':-1, 'winner':'()'}
        
        elif index == -1 :
            # conditions not met
            self.log("No condition met", level='warning')
            return None
        else:
            return {'i':index, 'priority':priority, 'winner':winner}

    def evaluateSchedule(self, i):
        '''Just tell the requested schedule value at the current time'''
        # We'not using the current hour, but hour of the last update. This is to enable time-testing
        hour           = self.conditions.now.hour   
        state          = self.schedules[i]['schedule'][hour]
        return state
            
    def toggle(self):
        '''Toggle switch to the correct state. Assuming the instance has the relevant switch/selector object'''
        if self.mode != 'manual':              # In manual mode, we do nothing
            order = self.state
            if order != None:                  # Order is None, we do nothing
                if self.inverted: order = 1 - order
                
                if self.object == 'selector':
                    # Case, we're talking to a Eco / Comfort selector switch
                    [ selector.set( order ) for selector in self.selectors ]
                    
                elif self.object == 'switch':
                    # We're talking to a switch
                    [ switch.set( order ) for switch in self.switches ]
                        
                else:
                    # If object is not Selector or Switch, can not handle
                    self.log("Unknown object: {}".format(self.object), level='warning')

    def print(self):
        '''Prints out schedules in a nice format'''
        def displayState(state):
            if state==0: return '___'
            if state==1: return 'HHH'
            if state not in [0,1]: return ("   " + str(state))[-3:]
        
        print( '\nName: ' , self.name,  '\tID: '   , self.ID, '\tMode:' , self.mode, '\tInverted: ' , self.inverted )
        print( 'Schedules: ' )
        
        print( ''.join( [ ('  ' + str(i))[-3:] for i in range(24) ] ) , '      --Conditions--'  )
        for schedule in self.schedules:
            print( ''.join( [displayState(item) for item in schedule['schedule']]) , '   ', schedule['conditions'] )







#-----------------------------------------------------------------------------------------------------------
#            DEVICES CLASS  (TOP LEVEL CLASS, CONTAINS THE PARSER)
#-----------------------------------------------------------------------------------------------------------



class DEVICES(CONTAINER):
    '''Contains the parser and all devices class instances'''
    def __init__(self, scheduleFile , scheduleTab, verbose=False, dry_run=False ):
        CONTAINER.__init__(self)
        self.name = 'parser'
        self.scheduleFile = scheduleFile
        self.scheduleTab  = scheduleTab
        self.conditions   = CONDITIONS(verbose=verbose)
        self.verbose      = verbose
        self.dry_run      = dry_run


        # Spreadsheet columns for the parser 
        self.INDEX_TITLES       = 1
        
        self.INDEX_NAME         = 1
        self.INDEX_ID           = 2
        self.INDEX_OBJECT       = 3     
        self.INDEX_MODE         = 4
        self.INDEX_INVERTED     = 5  
        self.INDEX_SCHEDULE     = [7, 30]        
        self.INDEX_CONDITIONS   = 33

        self.INDEX_COND_PRIORITY = 2
        self.INDEX_COND_NAME     = 3
        self.INDEX_COND_VALUE    = 4

        # Opening spreadsheet and running parser............................................
        try:
            self.spreadsheet = pyexcel_ods.get_data(self.scheduleFile)[self.scheduleTab]
        except:
            self.log("Can't open spreadsheet file", level='error', errortype=FileNotFoundError)

        self.parse_spreadsheet()

    @property
    def devices(self):
        return self.STORE

    def __repr__(self):
        return "{} devices".format(len(self))
    
    def __call__(self, ID ):
        '''Enables to access a device by :
            - Its rank in the table, e.g. 4
            - A part of its name'''

        if type(ID) == type(0) : return self.devices[ID]
        if type(ID) == type(''): 
            out = []
            # Finding all devices which name contain string "ID"
            for device in self:
                if ID in device.name : out.append( device )
            if len(out) >= 1: return out[0]    # Found, returning first of them
            else:             return None      # Not found, the device does not exist

    def print(self):
        '''Print schedules for all devices'''
        for device in self: device.print()

    def parse_spreadsheet(self):
        '''This is the parser'''

        # Useful functions, local to the parser function
        def sheetCell(Line, Row):
            '''Present an infinite-size sheet
                - Line has to be a positive integer
                - Row can be an integer or a range e.g. [5, 21]'''
            if Line >= N:
                return ''
            else:
                if type(Row) == type([]):
                    # Returning requested table
                    return [ sheetCell(Line, i) for i in range( Row[0], Row[1] + 1 ) ]  # Row[1] + 1: including last index
                else:
                    # Returning requested cell
                    if Row >= len(self.spreadsheet[Line] ):
                        return ''
                    else:
                        cellContent = self.spreadsheet[Line][Row]
                        if type( cellContent ) == type(''):  return cellContent.strip()
                        else:                                return cellContent

        def appendSchedule(schedules, i ):
            '''Append spreadsheet line to the schedule table'''
            #Convert schedule received from the spreadsheet eg: '', ' ', '' to useable info 0,1,0,1,1'''
            schedule = []
            for element in sheetCell(i, self.INDEX_SCHEDULE):
                if type(element) == type(''):
                    # empty or 'x'
                    if element == ''          : schedule.append( 0 )
                    if element.lower() == 'x' : schedule.append( 1 )                    

                if type( element ) == type(0):
                    # int
                    schedule.append( element )

            #Convert conditions received from the spreadsheet to table
            conditions = []
            for element in sheetCell(i,self.INDEX_CONDITIONS).split(','):
                if element.strip() != '': conditions.append( element.strip() )
            
            schedules.append( {'i':len(schedules), 'schedule':schedule, 'conditions':conditions  } )            
            
        # Parsing.............................................................................
        i = 0
        N = len(self.spreadsheet)

        while i < N and str(sheetCell(i,self.INDEX_TITLES)) != 'Info':
            i += 1    # Skipping to Infos

        i+=2 # On 'infos', skipping header

        self.info = {}
        while i < N and str(sheetCell(i,self.INDEX_TITLES)) != 'Device':
            field = str(sheetCell(i,self.INDEX_ID))
            value = str(sheetCell(i,self.INDEX_OBJECT))
            if field != '' : self.info[field] = value
            i += 1   

        i += 1  # On first line
        while( sheetCell(i,self.INDEX_NAME) == '' ): i += 1   # skipping blank lines

        # On first device
        while i < N and str(sheetCell(i,self.INDEX_TITLES)) != 'Conditions':
            # Capturing new device...................................................
            # Name
            name      = str(sheetCell(i,self.INDEX_NAME)).strip()                           # name as a string.

            # ID
            try:
                ID        = [ int(IDx) for IDx in str(sheetCell(i,self.INDEX_ID)).split(',')]   # ID as an int or str
            except ValueError:
                self.log( "Expecting coma-separated integers for field <ID> of device: " + name, level='error', errortype=ValueError )

            # Object and mode   
            object    = str(sheetCell(i,self.INDEX_OBJECT)).strip().lower()                      # str
            mode      = str(sheetCell(i,self.INDEX_MODE)).strip().lower()                        # str

            #'inverted' field
            inverted  = sheetCell(i,self.INDEX_INVERTED)                                    # bool
            if type(inverted) != type(True):
                self.log("Expecting boolean for field <inverted> of device: " + name, level='error', errortype=ValueError)
            
            schedules = []; appendSchedule(schedules, i)
            i += 1
            
            # Capturing more schedules and conditions, if any
            while i < N and sheetCell(i,self.INDEX_CONDITIONS) != '':
                appendSchedule(schedules, i)
                i+=1
                
            # Done. Storing device
            self.STORE.append( DEVICE(name, ID, object, mode, inverted, schedules, verbose=self.verbose) )
            self.log("Found device: " + name )
            
            # Done one device, skipping to the next
            while i < N and sheetCell(i,self.INDEX_NAME) == '':  i += 1      

        # On "Conditions", skipping to first condition
        i += 2          # skipping titles
        while i < N and str(sheetCell(i,self.INDEX_COND_PRIORITY)) == '': i += 1       # skipping spaces
        
        while i < N and str(sheetCell(i,self.INDEX_TITLES)) != 'Help':
            # Name
            name     = str(sheetCell(i,self.INDEX_COND_NAME    )).strip()   # str
            if ',' in name:
                self.log("Condition name should not contain comas: " + name, level='error', errortype=ValueError )

            if name != '':      # If name is filled up, trying to capture priority and value
                # Priority
                try:
                    priority = int(sheetCell(i,self.INDEX_COND_PRIORITY))           # int
                except ValueError:
                    self.log("Expecting integer for field <priority> for condition: " + name, level='error', errortype=ValueError )

                # Value
                value    = sheetCell(i,self.INDEX_COND_VALUE   )           # bool
                if type(value) != type(True):
                    self.log("Expecting boolean for field <value> for condition: " + name, level='error', errortype=ValueError )

                self.conditions.update_condition( name, value, priority   )

            i += 1

        #  Reached 'Help', finished parsing
        
        # Propagating conditions structure to devices, so that they can use it................................................
        for device in self:
            device.conditions = self.conditions

        # Creating a switch/thermostat instance for each device
        for device in self:
            device.switches    = [ SWITCH( urlbase, username, passwd, IDx, verbose=self.verbose, dry_run=self.dry_run ) for IDx in device.ID ]
            device.selectors   = [ SELECTOR_SWITCH( urlbase, username, passwd, IDx, {10:True, 20:False}, verbose=self.verbose, dry_run=self.dry_run ) for IDx in device.ID ]

    def toggle(self):
        '''Toggle all devices to the correct state. Propagating to device level'''
        for device in self: device.toggle()

    def report(self):
        '''Print state of all devices in a nice way'''
        # Finding version number for report
        try:         v = version
        except:      v = "?.?"

        # Printing date and such
        print( "{}, Script v{} {} {}\n".format(self.conditions.now.strftime('%d, %b %Y - %H:%M'), v, ", <<  DRY RUN  >>" * self.dry_run, '-'*20 ) )

        # Printing sheet info
        for field in self.info:
            print("{} : {}".format(field, self.info[field] ) )

        # Listing TRUE conditions
        print("\nConditions: {}".format(self.conditions.TrueConditions() ))

        # Printing devices states
        print('\n')
        print( (' '*40 + 'DEVICE NAME:')[-40:] , 'STATE', '(MODE Condition)' )
        for device in self:
            if device.mode == 'prog':
                if device.winningSchedule() != None:
                    #Finding winning schedule
                    schedule = " {}".format(device.winningSchedule()['winner'])
                else:
                    schedule = ' !!'
            else:
                schedule = ''
            print( (' '*40 + device.name + ':')[-40:], device.state, '(' + device.mode + schedule + ')' )
        print()



#-----------------------------------------------------------------------------------------------------------
#            MAIN 
#-----------------------------------------------------------------------------------------------------------


if __name__ == '__main__':      
    # Creating Domoticz notification instance
    notification = NOTIFICATION(urlbase, username, passwd, None, verbose=False, dry_run=dry_run )
    
    # Placing the whole program within a try in order to catch ANY error and notify via SMS
    # if the error was caught within the program, the user will be notified twice
    try:        
        # opening schedule
        devices = DEVICES( scheduleFile , scheduleTab, verbose=False, dry_run=dry_run)

        # Toggling devices and reporting
        devices.toggle()
        devices.report()

    except:
        notification.notify("Schedule script", "Schedule script. An issue has occurred")
        
