#
#  Enters an infinite loop and runs the schedule script: 
#    - on a timer, every hour
#    - if Schedule spreadsheet is modified
#    - upon request, via a naled pipe (/tmp/loopPipe )


import time, datetime, hashlib, os, pipes
from threading import Thread
from subprocess import call, run, PIPE, STDOUT

from Parameters import *  # Parameters for the script, entered as python variables

CMD = ["python", "Schedule.py"]

class BaseClass:
    def __init__(self):
        pass
    
    def log(self, m):
        print( "{} --- ({})".format(datetime.datetime.now().strftime('%d %b - %H:%M'), m) )

    def run_script(self, CMD):
        #print('-'*50, ">> Calling prog", '-'*50)
        #p = run( CMD, stdout=PIPE, stderr=STDOUT )
        #for line in str(p.stdout).split('\n'):
        #    print(line )
        call( CMD )

class Timer(Thread, BaseClass):
    '''Runs the script every hour, at x:01'''
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        self.log( "Initial run")
        while True:
            # Running script
            self.log( "Triggered by timer" )
            self.run_script(CMD)                          # <----  running schedule script

            # Waiting for next period
            now = datetime.datetime.now()
            time.sleep(60 * (60 - now.minute +1) )


class FileUpdate(Thread, BaseClass):
    '''Runs the script if the Schedule is updated on disk'''
    def __init__(self, path, RequestContentChange=True):
        Thread.__init__(self)
        self.path                 = path
        self.RequestContentChange = RequestContentChange

    def run(self):
        # Initial modification date and hash
        if not os.path.exists( self.path ):
            self.log( "Schedule file not found.." )
            while not os.path.exists( self.path ): time.sleep(1)   # Wait for file to appear
            self.log( "Schedule file back again" )
        
        LastModifValue = self.LastModif()
        LastMD5Value   = self.MD5()
        
        while True:
            if os.path.exists(self.path) and self.LastModif() > LastModifValue:
                #File was updated
                                
                if self.MD5() != LastMD5Value or not(self.RequestContentChange):                  
                    self.log( "Triggered by file update" )
                    self.run_script(CMD)                                 # <----  running schedule script
                else:
                    self.log( "File updated, no modification" )
                
                #Storing modification date and hash for next time
                if os.path.exists( self.path ):
                    LastModifValue = self.LastModif()
                    LastMD5Value = self.MD5()

            if not os.path.exists(self.path):
                self.log( "Schedule file not found.." )
                while not os.path.exists(self.path): time.sleep(1)  # Wait for file to appear
                self.log( "Schedule file back again" )
                
            time.sleep(1)

    def LastModif(self):
        return os.path.getmtime(self.path)

    def MD5(self):
        hash_md5 = hashlib.md5()
        with open(self.path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()


class RequestedUpdate(Thread, BaseClass):
    '''Runs the script if requested'''
    def __init__(self, pipe='/tmp/loopPipe'):
        Thread.__init__(self)
        self.pipe = pipe

    def run(self):
        # initial pipe opening
        if not os.path.exists(self.pipe):
            os.mkfifo(self.pipe)
        self.f = open(self.pipe, 'r')  # Blocking until a program writes in it

        # Loop
        while True:
            if os.path.exists(self.pipe):
                if self.f.closed: self.f = open(self.pipe, 'r')  
                
                for order in self.f.readlines():  # Assuming file exists!
                    order = order.strip()
                    if order == 'up':
                        self.log( "Triggered by request" )
                        self.run_script(CMD)                                # <----  running schedule script

                    else:
                        self.log( "Don't understand '{}'".format(order) )
            else:
                os.mkfifo(self.pipe)
                self.f = open(self.pipe, 'r')  # Blocking until a program writes in it

            time.sleep(1)

if __name__ == '__main__':
    t = Timer()
    t.start()
    
    FU = FileUpdate(scheduleFile, RequestContentChange=True )
    FU.start()

    RU = RequestedUpdate()
    RU.start()

