## Purpose and principle of the program

The purpose of the script is to enable easy scheduling of Domoticz switches across several conditions. For example, I need the living room radiator to heat up in some circumstances (e.g. in the evening in a normal work day) but at other times during particular days (full day during home office or week end). But wait, sometimes Grandpa and Grandma also come in the afternoon, so they need heating only in the afternoon!

Domoticz does provide a way to handle these needs, but a fine configuration gets quickly very complicated using **Timers** and **Timer Plans**.

With this script instead, the whole set of configurations sits in a single spreadsheet, that is very easy to manipulate, with LibreOffice for example. Every hour, the script parses the spreadsheet, thus extracting the scheduling information. Based on that information, it decides what should be the state of each device, and sets it in Domoticz.

The spreadsheet has a 1-hour granularity to keep things readable, hence the switches are updated at that frequency.

![Spreadsheet](Screen_capture.png)





## Getting started

Here is a quick description of the files included. Follows how to use them.

- **loop.py**               : Main loop. Runs the Schedule.py script every hour or if the Spreadsheet has been updated

- **Schedule.py**           : Schedule script (main file)
- **Schedule.ods**          : Schedule spreadsheet
- **Parameters.py**         : Script parameters, Domoticz credentials and such
- **Domoticz.py**           : Code for handling connection to domoticz
- **Schedule_tests.py**     : Demo/Test


**First** install pyexcel_ods. This library is required by the Schedule script

**Second**, open Parameters.py.FILLUPFIRST, fill up variables, and save to Parameters.py

The script can now be ran in a terminal, via the following command: python3 Schedule.py

**Third**. Modify spreadsheet acording to your needs, and rerun the script

**Fourth** When happy, change *dry_mode=True* to *dry_mode=False* in Parameters.py, to test the connection to Domoticz. You're then good to go.


The Schedule script runs once and then quits, hence can be executed via cron (for Linux) instead of the loop.py script
As the schedules have a 1h-precision, it's best to run the Schedule script every hour, at one minute past the hour.
> 01 * * * * python3 /home/domoticz/Schedule.py > /tmp/Schedule.log


## Docker

We provide a docker image together with the Schedule script. It it set to run loop.py upon startup.
The necessary files are in the 'image' subfolder. docker-compose.yml file provided as an example.


### Devices

Currently, the script only handles two types of Domoticz devices: **Selector** and **Switch**

 * **Selector**: Sets ECO/CONFORT modes via the SVT plugin (https://www.domoticz.com/wiki/Plugins/Smart_Virtual_Thermostat.html). Note that the script can be easily modified to accomodate for other uses with selectors having two positions.
 * **Switch**: Usual Domoticz ON/OFF switch. 

### Mode and Schedule

Like in any heating system, the devices can follow a schedule or set to fixed states:

* **ON**: Forced ON
* **OFF**: Forced OFF
* **Manual**: No order sent to Domoticz
* **Prog**: Follows the schedule entered in the spreadsheet

### Conditions

To accomodate for the situations already mentioned (heater needs to be ON at certain times, depending on conditions), a whole system with conditions and priorities was put in place. The system is based on default conditions, and user-defined conditions, each having priorities.

**Default conditions.** By default, the following conditions are set up:

* **WD**: "Week Day", true on a Week day
* **WE** "Week End"
* Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday

    **All default conditions come with a priority of 0** 

**User-defined conditions.** User-defined conditions can be entered in the spreadsheet, and changed when required. Examples are "HomeOffice", "Holidays", "GrandParents", "Away".. They can be used to reflect the various modes.
The parameters required are:

* **Name**: A simple name to identify the condition (no spaces nor comas in the name allowed)
* **Priority**: Give a priority to this condition with respect to the others. This serves in case of competing conditions (e.g. Holidays vs HomeOffice). **A higher figure means higher priority**
* **Value**: TRUE or FALSE

**Principle of operation** For a given device, several schedules can be entered, each with an associated set of conditions. Here is an example of a simple specification:

* Evening hours (=schedule) on a week day (=condition)
* whole day (=schedule) in the week end (condition)


In case several conditions are given for a particular schedule, any matching condition will enable the associated schedule.
In case several schedules are enabled for that device, the highest priority wins. Finally, if several schedules score the same priority, the first appearing on the spreadsheet wins.

On the other hand, if no conditions are given, the schedule is considered to be true all the time.








## Details on the spreadsheet structure

The spreadsheet contains two main sections, *Device* and *Conditions*, that are detailed below. Each contain subsections, also detailed.

Other sections appear on the spreadsheet that need not be detailed: *info*, enables the user to pass variables to the program, and *help*.


### Warning

While reading the spreadsheet, the script expects the information to be at certain places, and with a certain order. Make sure you don't move elements to incorrect places, as this would lead to incorrect reading or program crash.

Also, because the program is not fully polished, it might crash without giving useful information about the issue. In that case, you'll have to look in to the code and debug manually.

Better be cautious.

### Device section

In this section are listed the devices to be driven by the script. The following information are required to drive the Domoticz objects:

* **Device:** Give a clear name to the device, e.g. "Kitchen heater"
* **Domoticz ID:** Give one **or several device IDs** to be actuated. If several IDs are given, they should be separated by comas
* **Domoticz object:** Give the object type: "Selector" or "Switch"
* **Mode:**  ON / OFF / Manual / Prog

* **Inverted:**   TRUE / FALSE

Each device specification can be separated by a blank line. 


**Note on inversion**  Domoticz does not provide inverted switches. Those would be useful in the case of a pilot wire driven by a simple switch, which results of an inversion in the command (0=> Comfort, 1=> Eco), as explained here: https://github.com/domoticz/domoticz/issues/846

### Conditions section

List the user-defined conditions there, as explained earlier.
Conditions are to be adjusted dayly depending on the mode wanted. They are taken into account by the script once the sheet has been saved.

### Spreadsheet automation

Note that the script can only read the raw text present in the spreadsheet. It does not have access to the formulas or formatting.
However, some level of automation is possible through the spreadsheet's functions, e.g. adding check boxes and buttons triggering macros.

**Just be aware that the contents are not updated while the script reads in the sheet, so functions like now() will always return the date the spreadsheet was saved.**

### Spreadsheet sharing

I have the sheet sitting on a cloud, that is accessible from everywhere.












## Script design

### Foreword
The script is at a very initial state. Especially for the parser part, it means that a lot of input-checking would need to be done to make it robust.
This is absolutely not the case at the moment, so one must be very careful while editing the spreadsheet.

### Script structure

The script is based on various classes. They are detailed below:

#### Container class
This class Manages a collection of objects, and provides simple methods to browse through them (iteration, indexing).
It is used throughout the script to handle lists of objects. As all other classes derive from it, we also place there functions common to every class, amongst which is logging.

The log function enables to record events behaving according to the log level declared, "log", "warning" or "error". Logging behaviour is defined there for all classes, and made such that:

* Messages are always printed to the terminal if level is "warning" or "error". They are printed in "log" level only if in verbose mode.
* Messages are sent via the Domoticz notification system if level  "warning" or "error"
* Errors cause an exception to be sent.

#### Conditions class

This class stores all up-to-date conditions, and provides function to enable decision on the current conditions.
One can pass a set of conditions, and check if this particular set enables the schedule, and at what priority.

#### Device class

This class holds all information related for a particular device, given from the user through the spreadsheet (devide name, ID, ... , schedules and conditions).
It provides functions to find the winning schedule and toggle the device accordingly.

#### Devices (with an 's') class

This is the top-most class. It contains the parser, and a list of all the devices declared in the spreadsheet.
All the top-level operations occur on this class, like toggling all devices at a time.

Any device stored in this class can be accessed via devices('name')

#### Main program

**Operation** When the script is ran, the spreadsheet is parsed, the "schedule" class instance is created, and all devices get updated.
Note that the script updates the Domoticz devices once, and then quits. It does not loop, this has to be done at a higher level (Linux, for instance)

**Error handling and notifications** Some error handling is done in the script. The parser, for instance, checks that all fields read in the spreadsheet have the right type and format.
Not all checks are done, but the main section is written so that, if any exception has occurred within the code, a Domoticz notification is fired. At least, the admin is notified.









## Testing

There are no systematic test routines for now.

One can test the script without having an actual instance of Domoticz running by putting the dry_run variable to True. This proves useful to test without messing with the house heating, for instance!

Additionally, the side script *Schedule_test.py* provides some level of debugging, by printing useful elements about the devices, schedules, conditions, and such. It enables to make sure that all info put in the spreadsheet were correctly parsed.

Additionally, if no error is found at runtime, it suggests that the main script will run smoothly too.








## Known limitations and future additions

**Update of unchanged states**  All the devices get updated every time the script is ran, even if their state is unchanged. This creates unnecessary lines in the Domoticz log, and probably also  unnecessary radio transmissions (TBC).

**Some ideas for future improvements**

* Give access to more Domoticz devices, e.g. level selector (1, 2, 3, .. 100%)
>This should be easy
* Provide more symbols for a finer control of time steps.
> E.g. '<' and '>' for half hours instead of 'x' for a full hour
* Get conditions value (TRUE/FALSE) from a domoticz Switch as an option
> For next revision :)
* conditions follow a weekly planning
